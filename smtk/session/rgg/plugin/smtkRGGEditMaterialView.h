//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME smtkRGGEditMaterialView - UI component for Edit RGG pins
// .SECTION Description
// .SECTION See Also
// qtSection

#ifndef smtkRGGEditMaterialView_h
#define smtkRGGEditMaterialView_h

#include "smtk/extension/qt/qtOperationView.h"
#include "smtk/session/rgg/operators/EditMaterial.h"
#include "smtk/session/rgg/plugin/Exports.h"

class QColor;
class QComboBox;
class QIcon;
class QString;
namespace smtk
{
namespace extension
{
class qtItem;
}
}
class smtkRGGEditMaterialViewInternals;

class smtkRGGEditMaterialView : public smtk::extension::qtBaseAttributeView
{
  Q_OBJECT

public:
  smtkTypenameMacro(smtkRGGEditMaterialView);
  smtkRGGEditMaterialView(const smtk::view::Information& info);
  virtual ~smtkRGGEditMaterialView();
  const smtk::operation::OperationPtr& operation() const;

  static smtk::extension::qtBaseView* createViewWidget(const smtk::view::Information& info);

public Q_SLOTS:
  void requestModelEntityAssociation() override;
  void onShowCategory() override { this->updateUI(); }
  void valueChanged(smtk::attribute::ItemPtr optype) override;

  void launchNuclideTable();

protected Q_SLOTS:
  virtual void requestOperation(const smtk::operation::OperationPtr& op);

  virtual void materialChanged(const QString& text);

  // This slot is used to indicate that the underlying attribute
  // for the operation should be checked for validity
  virtual void attributeModified();
  void apply();

protected:
  void updateUI() override;
  void createWidget() override;
  bool ableToOperate();
  void updateEditMaterialPanel();
  virtual void setInfoToBeDisplayed() override;
  void setupMaterialComboBox(QComboBox* box);
  void setupDensityTypeComboBox(QComboBox* box);
  void setupCompositionTypeComboBox(QComboBox* box);
  void clear();
  void setEnabled(bool);

private:
  smtkRGGEditMaterialViewInternals* Internals;
};

#endif // smtkRGGEditMaterialView_h

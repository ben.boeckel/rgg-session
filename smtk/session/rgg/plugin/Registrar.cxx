//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/session/rgg/plugin/Registrar.h"

#include "smtk/session/rgg/plugin/smtkRGGAddMaterialView.h"
#include "smtk/session/rgg/plugin/smtkRGGEditAssemblyView.h"
#include "smtk/session/rgg/plugin/smtkRGGEditCoreView.h"
#include "smtk/session/rgg/plugin/smtkRGGEditDuctView.h"
#include "smtk/session/rgg/plugin/smtkRGGEditMaterialView.h"
#include "smtk/session/rgg/plugin/smtkRGGEditPinView.h"
#include "smtk/session/rgg/plugin/smtkRGGRemoveMaterialView.h"

namespace smtk
{
namespace session
{
namespace rgg
{
namespace plugin
{
namespace
{
typedef std::tuple<smtkRGGAddMaterialView, smtkRGGEditAssemblyView,
                   smtkRGGEditCoreView, smtkRGGEditDuctView,
                   smtkRGGEditMaterialView, smtkRGGEditPinView,
                   smtkRGGRemoveMaterialView>
    ViewWidgetList;
}

void Registrar::registerTo(const smtk::view::Manager::Ptr& viewManager)
{
  viewManager->viewWidgetFactory().registerTypes<ViewWidgetList>();
}

void Registrar::unregisterFrom(const smtk::view::Manager::Ptr& viewManager)
{
  viewManager->viewWidgetFactory().unregisterTypes<ViewWidgetList>();
}

}
}
}
}

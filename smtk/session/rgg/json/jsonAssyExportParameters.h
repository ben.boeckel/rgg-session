//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_rgg_json_jsonAssyExportParameters_h
#define smtk_session_rgg_json_jsonAssyExportParameters_h

#include "smtk/session/rgg/Exports.h"

#include "smtk/session/rgg/meshkit/AssyExportParameters.h"

#include "nlohmann/json.hpp"

namespace smtk
{
namespace session
{
namespace rgg
{

SMTKRGGSESSION_EXPORT void to_json(
  nlohmann::json& j, const AssyExportParameters& aep);

SMTKRGGSESSION_EXPORT void from_json(
  const nlohmann::json& j, AssyExportParameters& aep);

} // namespace rgg
} // namespace session
} // namespace smtk
#endif

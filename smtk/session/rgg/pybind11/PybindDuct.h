//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_rgg_Duct_h
#define pybind_smtk_session_rgg_Duct_h

#include <pybind11/pybind11.h>

#include "smtk/session/rgg/Duct.h"
#include "smtk/model/AuxiliaryGeometry.h"
#include "smtk/session/rgg/json/jsonDuct.h"
#include <pybind11/stl.h>

namespace py = pybind11;

py::class_< smtk::session::rgg::Duct > pybind11_init_smtk_session_rgg_Duct(py::module &m)
{
  py::class_< smtk::session::rgg::Duct > instance(m, "Duct");
  instance
    .def(py::init<>())
    .def(py::init<::std::string const &, bool>())
    .def(py::init<::smtk::session::rgg::Duct const &>())
    .def(py::init([](smtk::model::AuxiliaryGeometry& ductAux)
        { return nlohmann::json::parse(ductAux.stringProperty(smtk::session::rgg::Duct::propDescription)[0]);}))
    .def("__eq__", (bool (smtk::session::rgg::Duct::*)(::smtk::session::rgg::Duct const &) const) &smtk::session::rgg::Duct::operator==)
    .def("__ne__", (bool (smtk::session::rgg::Duct::*)(::smtk::session::rgg::Duct const &) const) &smtk::session::rgg::Duct::operator!=)
    .def("deepcopy", (smtk::session::rgg::Duct & (smtk::session::rgg::Duct::*)(::smtk::session::rgg::Duct const &)) &smtk::session::rgg::Duct::operator=)
    .def("name", &smtk::session::rgg::Duct::name)
    .def("isCutAway", &smtk::session::rgg::Duct::isCutAway)
    .def("segments", (std::vector<smtk::session::rgg::Duct::Segment, std::allocator<smtk::session::rgg::Duct::Segment> > & (smtk::session::rgg::Duct::*)()) &smtk::session::rgg::Duct::segments)
    .def("segments", (std::vector<smtk::session::rgg::Duct::Segment, std::allocator<smtk::session::rgg::Duct::Segment> > const & (smtk::session::rgg::Duct::*)() const) &smtk::session::rgg::Duct::segments)
    .def("setName", &smtk::session::rgg::Duct::setName, py::arg("name"))
    .def("setCutAway", &smtk::session::rgg::Duct::setCutAway, py::arg("cutAway"))
    .def("setSegments", &smtk::session::rgg::Duct::setSegments, py::arg("segments"))
    .def_property_readonly_static("propDescription", [](py::object){ return std::string(smtk::session::rgg::Duct::propDescription);})
    .def_property_readonly_static("typeDescription", [](py::object){ return std::string(smtk::session::rgg::Duct::typeDescription);})
    .def_property_readonly_static("segmentDescription", [](py::object){ return std::string(smtk::session::rgg::Duct::segmentDescription);})
    .def_property_readonly_static("layerDescription", [](py::object){ return std::string(smtk::session::rgg::Duct::layerDescription);})
    ;
  py::class_< smtk::session::rgg::Duct::Segment >(instance, "Segment")
    .def(py::init<>())
    .def(py::init<::smtk::session::rgg::Duct::Segment const &>())
    .def("deepcopy", (smtk::session::rgg::Duct::Segment & (smtk::session::rgg::Duct::Segment::*)(::smtk::session::rgg::Duct::Segment const &)) &smtk::session::rgg::Duct::Segment::operator=)
    .def_readwrite("baseZ", &smtk::session::rgg::Duct::Segment::baseZ)
    .def_readwrite("height", &smtk::session::rgg::Duct::Segment::height)
    .def_readwrite("layers", &smtk::session::rgg::Duct::Segment::layers)
    ;
  return instance;
}

#endif

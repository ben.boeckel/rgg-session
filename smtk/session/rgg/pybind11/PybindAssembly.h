//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_rgg_Assembly_h
#define pybind_smtk_session_rgg_Assembly_h

#include <pybind11/pybind11.h>

#include "smtk/session/rgg/Assembly.h"
#include "smtk/common/pybind11/PybindUUIDTypeCaster.h"
#include "smtk/model/Group.h"
#include "smtk/session/rgg/json/jsonAssembly.h"
#include <pybind11/stl.h>

namespace py = pybind11;

py::class_< smtk::session::rgg::Assembly > pybind11_init_smtk_session_rgg_Assembly(py::module &m)
{
  py::class_< smtk::session::rgg::Assembly > instance(m, "Assembly");
  instance
    .def(py::init<>())
    .def(py::init<::std::string const &, ::std::string const &>())
    .def(py::init<::smtk::session::rgg::Assembly const &>())
    .def(py::init([](smtk::model::Group& assyG)
        { return nlohmann::json::parse(assyG.stringProperty(smtk::session::rgg::Assembly::propDescription)[0]);}))
    .def("__eq__", (bool (smtk::session::rgg::Assembly::*)(::smtk::session::rgg::Assembly const &) const) &smtk::session::rgg::Assembly::operator==)
    .def("__ne__", (bool (smtk::session::rgg::Assembly::*)(::smtk::session::rgg::Assembly const &) const) &smtk::session::rgg::Assembly::operator!=)
    .def("deepcopy", (smtk::session::rgg::Assembly & (smtk::session::rgg::Assembly::*)(::smtk::session::rgg::Assembly const &)) &smtk::session::rgg::Assembly::operator=)
    .def_static("isAnUniqueLabel", &smtk::session::rgg::Assembly::isAnUniqueLabel, py::arg("label"))
    .def_static("generateUniqueLabel", &smtk::session::rgg::Assembly::generateUniqueLabel)
    .def("name", &smtk::session::rgg::Assembly::name)
    .def("label", &smtk::session::rgg::Assembly::label)
    .def("color", &smtk::session::rgg::Assembly::color)
    .def("zAxis", &smtk::session::rgg::Assembly::zAxis)
    .def("rotate", &smtk::session::rgg::Assembly::rotate)
    .def("layout", (smtk::session::rgg::Assembly::UuidToSchema & (smtk::session::rgg::Assembly::*)()) &smtk::session::rgg::Assembly::layout)
    .def("layout", (smtk::session::rgg::Assembly::UuidToSchema const & (smtk::session::rgg::Assembly::*)() const) &smtk::session::rgg::Assembly::layout)
    .def("entityToCoordinates", (smtk::session::rgg::Assembly::UuidToCoordinates & (smtk::session::rgg::Assembly::*)()) &smtk::session::rgg::Assembly::entityToCoordinates)
    .def("entityToCoordinates", (smtk::session::rgg::Assembly::UuidToCoordinates const & (smtk::session::rgg::Assembly::*)() const) &smtk::session::rgg::Assembly::entityToCoordinates)
    .def("associatedDuct", &smtk::session::rgg::Assembly::associatedDuct)
    .def("centerPin", &smtk::session::rgg::Assembly::centerPin)
    .def("pitch", &smtk::session::rgg::Assembly::pitch)
    .def("latticeSize", &smtk::session::rgg::Assembly::latticeSize)
    .def("exportParams", (smtk::session::rgg::AssyExportParameters & (smtk::session::rgg::Assembly::*)()) &smtk::session::rgg::Assembly::exportParams)
    .def("exportParams", (smtk::session::rgg::AssyExportParameters const & (smtk::session::rgg::Assembly::*)() const) &smtk::session::rgg::Assembly::exportParams)
    .def("setName", &smtk::session::rgg::Assembly::setName, py::arg("name"))
    .def("setLabel", &smtk::session::rgg::Assembly::setLabel, py::arg("label"))
    .def("setColor", &smtk::session::rgg::Assembly::setColor, py::arg("color"))
    .def("setZAxis", &smtk::session::rgg::Assembly::setZAxis, py::arg("zAxis"))
    .def("setRotate", &smtk::session::rgg::Assembly::setRotate, py::arg("rotate"))
    .def("setLayout", &smtk::session::rgg::Assembly::setLayout, py::arg("schema"))
    .def("setEntityToCoordinates", &smtk::session::rgg::Assembly::setEntityToCoordinates, py::arg("uTC"))
    .def("setAssociatedDuct", &smtk::session::rgg::Assembly::setAssociatedDuct, py::arg("id"))
    .def("setCenterPin", &smtk::session::rgg::Assembly::setCenterPin, py::arg("centerPin"))
    .def("setPitch", &smtk::session::rgg::Assembly::setPitch, py::arg("p0"), py::arg("p1") = 0)
    .def("setLatticeSize", (void(smtk::session::rgg::Assembly::*)(const int&, const int&)) &smtk::session::rgg::Assembly::setLatticeSize, "Set the lattice size via two ints")
    .def("setLatticeSize", (void(smtk::session::rgg::Assembly::*)(const std::pair<int, int>&)) &smtk::session::rgg::Assembly::setLatticeSize, "Set the lattice size via a pair of int")
    .def("setExportParams", &smtk::session::rgg::Assembly::setExportParams, py::arg("aep"))
    .def_property_readonly_static("propDescription", [](py::object){ return std::string(smtk::session::rgg::Assembly::propDescription);})
    .def_property_readonly_static("typeDescription", [](py::object){ return std::string(smtk::session::rgg::Assembly::typeDescription);})
    ;
  return instance;
}

#endif

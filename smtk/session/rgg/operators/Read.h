//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_rgg_Read_h
#define smtk_session_rgg_Read_h

#include "smtk/session/rgg/Exports.h"

#include "smtk/operation/XMLOperation.h"

namespace smtk
{
namespace session
{
namespace rgg
{

/**\brief Read a smtk rgg model file.
 *
 * Here and throughout SMTK, we use the terms read/write to describe
 * serialization of native SMTK files, while the terms import/export describe
 * transcription from/to a different format. Currently, the rgg session's
 * model file is a .smtk json file that describes rgg entities.
 */
class SMTKRGGSESSION_EXPORT Read : public smtk::operation::XMLOperation
{
public:
  smtkTypeMacro(smtk::session::rgg::Read);
  smtkCreateMacro(Read);
  smtkSharedFromThisMacro(smtk::operation::Operation);

protected:
  Result operateInternal() override;
  virtual const char* xmlDescription() const override;
  void markModifiedResources(Result&) override;
};

SMTKRGGSESSION_EXPORT smtk::resource::ResourcePtr read(
  const std::string& filename,
  const std::shared_ptr<smtk::common::Managers>& managers = nullptr
);
}
}
}

#endif

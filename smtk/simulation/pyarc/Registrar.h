//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_simulation_pyarc_Registrar_h
#define __smtk_simulation_pyarc_Registrar_h

#include "smtk/simulation/pyarc/Exports.h"

#include "smtk/session/rgg/Registrar.h"

namespace smtk
{
namespace simulation
{
namespace pyarc
{

class SMTKRGGSIMULATIONPYARC_EXPORT Registrar
{
public:
  using Dependencies = std::tuple<session::rgg::Registrar>;

  static void registerTo(const smtk::operation::Manager::Ptr&);
  static void unregisterFrom(const smtk::operation::Manager::Ptr&);

  static void addModuleToPythonPath();
};
}
}
}

#endif

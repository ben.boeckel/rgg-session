set (PyARCPluginHdrs )
set (PyARCPluginSrcs )
smtk_add_plugin(
  smtkRGGSimulationPyARCPlugin
  REGISTRAR smtk::simulation::pyarc::Registrar
  MANAGERS smtk::operation::Manager
  PARAVIEW_PLUGIN_ARGS
  VERSION "1.0"
  REQUIRED_PLUGINS
    smtkCore
    smtkRGGSession
    smtkRGGSimulationPyARC
  SOURCES
  ${PyARCPluginHdrs}
  ${PyARCPluginSrcs}
)

target_link_libraries(smtkRGGSimulationPyARCPlugin
  PRIVATE
    smtkRGGSimulationPyARC)
